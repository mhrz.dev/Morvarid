﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DevExpress.Xpf.Ribbon;
using DevExpress.Xpf.Bars;

namespace Morvarid
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : DXRibbonWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            
            ///adding eventHandler to bar button items.
            brbtnNewCustomers.ItemClick += BrbtnNewCustomers_ItemClick;//add event for clicking in brbtnNewCustomers.


        }
       public    frmNewCustomer fNewCustomer = new frmNewCustomer();

        private void BrbtnNewCustomers_ItemClick(object sender, ItemClickEventArgs e)
        {
            fNewCustomer.Show();
        }




        
        public void BrbtnNewCustomers(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            MessageBox.Show("Item " + e.Item.Content + " has been clicked.");

        
            
        }
    }
}
